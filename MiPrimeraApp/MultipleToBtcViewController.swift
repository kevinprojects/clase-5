//
//  MultipleToBtcViewController.swift
//  MiPrimeraApp
//
//  Created by Kevin Belter on 11/22/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class MultipleToBtcViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var monedasPickerView: UIPickerView!
    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var lblResultado: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monedasPickerView.dataSource = self
        monedasPickerView.delegate = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return divisas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return divisas[row]
    }
    
    @IBAction func tocoConvertir() {
        
        let rowSeleccionada = monedasPickerView.selectedRow(inComponent: 0)
        let divisa = divisas[rowSeleccionada]
        
        let cotizador = Cotizador()
        cotizador.obtenerValorBTC(divisa: divisa) { valorBTCOpcional in
            if let valorString = self.txtValor.text, let valor = Double(valorString), let valorBTC = valorBTCOpcional {
                let resultado =  valor / valorBTC
                self.lblResultado.text = "\(resultado)"
            }
        }
        txtValor.resignFirstResponder()
    }
    
    private var divisas = ["ARS", "USD", "MXN", "EUR", "KAM", "POM"]
}
